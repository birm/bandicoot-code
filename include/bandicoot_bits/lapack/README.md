This directory contains high-level reimplementations of LAPACK functionality, as
found in MAGMA and clMAGMA.  The functions here can be used as building blocks
for higher-level functionality.

In some cases, the functions do not have all the functionality of the original
LAPACK functions---this is because the way they are used in Bandicoot does not
need this functionality.
